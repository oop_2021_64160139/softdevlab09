/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab09;

import com.mycompany.lab09.newpackage.model.User;
import com.mycompany.lab09.service.UserService;

/**
 *
 * @author User
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user1 = userService.login("user2", "password");
        if(user1 != null){
            System.out.println("Welcome user : "+ user1.getName());
        }else{
            System.out.println("Error");
        }
    }
}
